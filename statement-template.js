// playground requires you to assign document definition to a variable called dd

var dd = {
  content: [
    {
      alignment: "justify",
      columns: [
        [
          {
            svg: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1190.55 748.71"><defs><style>.cls-1{fill:#ca4e19;}</style></defs><path class="cls-1" d="M702.62,676.41a21.87,21.87,0,0,1,6.75,3.67,25.46,25.46,0,0,1,5.2,5.69,26.42,26.42,0,0,1,3.39,7.45,32.12,32.12,0,0,1,1.19,8.82,31,31,0,0,1-1.86,10.9,24.62,24.62,0,0,1-5.15,8.43,23,23,0,0,1-7.86,5.35,25.41,25.41,0,0,1-9.91,1.92q-9.74,0-15.27-6.68h-.2v26.75H665V676H678.9v6h.2q5.8-6.83,15.68-6.83a24.17,24.17,0,0,1,7.84,1.27m-1.24,35.88a14.3,14.3,0,0,0,3.7-10.19q0-6.52-3.78-10.5a12.49,12.49,0,0,0-9.47-4,13.39,13.39,0,0,0-5.46,1.11,12.62,12.62,0,0,0-4.26,3,13.85,13.85,0,0,0-2.77,4.6,16.32,16.32,0,0,0-1,5.75,15.1,15.1,0,0,0,1.78,7.39,12.41,12.41,0,0,0,4.87,5,13.78,13.78,0,0,0,6.85,1.73,12.53,12.53,0,0,0,9.55-3.93M781.16,676v51.74H768V721.5h-.26q-5.54,7.14-15.47,7.14a25.32,25.32,0,0,1-9.9-1.92,22.9,22.9,0,0,1-7.87-5.35,24.62,24.62,0,0,1-5.15-8.43,32.6,32.6,0,0,1,0-21.71,25.83,25.83,0,0,1,5.1-8.53,23.61,23.61,0,0,1,17.41-7.56,19.17,19.17,0,0,1,15.26,6.73h.21V676Zm-17.07,36.27a14.09,14.09,0,0,0,3.78-10.19,14.94,14.94,0,0,0-3.7-10.35,12,12,0,0,0-9.34-4.14,12.56,12.56,0,0,0-9.49,4q-3.81,4-3.81,10.5a14.3,14.3,0,0,0,3.7,10.19,12.53,12.53,0,0,0,9.55,3.93,12.4,12.4,0,0,0,9.31-3.93M843,676H828.64l-11.84,32h-.16L804,676H789.48l20.69,48.53L801,748.71h14.18L843,676Zm85.27,4.66q-4.78-5.53-13.48-5.54a24.05,24.05,0,0,0-10.22,2.15,20.92,20.92,0,0,0-7.78,6.18h-.16q-4.54-8.33-15.36-8.33a18.8,18.8,0,0,0-8.38,2,21.36,21.36,0,0,0-7.09,5.72h-.16V676H851.81v51.74h13.86V701.89a19.31,19.31,0,0,1,1.45-7.79,10.88,10.88,0,0,1,3.88-4.88,9.91,9.91,0,0,1,5.59-1.61q9.17,0,9.15,13.45v26.75h13.87V701.89A19.67,19.67,0,0,1,901,694.1a10.84,10.84,0,0,1,3.81-4.88,9.29,9.29,0,0,1,5.32-1.61q9.06,0,9.06,13.45v26.75h13.87V697.7q0-11.49-4.79-17m65.92,25.71H958a13,13,0,0,0,4.73,7.24,13.85,13.85,0,0,0,8.46,2.69,15.19,15.19,0,0,0,12.37-5.84l9,8.38A25,25,0,0,1,983.3,726a30.65,30.65,0,0,1-13.14,2.67,28.08,28.08,0,0,1-13.61-3.29,23.93,23.93,0,0,1-9.54-9.44,28,28,0,0,1-3.5-14.07,29.23,29.23,0,0,1,2-10.79,25.2,25.2,0,0,1,5.35-8.48,23.94,23.94,0,0,1,8.15-5.46,26.47,26.47,0,0,1,10.19-2,25.85,25.85,0,0,1,13.3,3.39,23.21,23.21,0,0,1,8.95,9.37,28.55,28.55,0,0,1,3.16,13.53,22.09,22.09,0,0,1-.36,5m-32-16.71a12,12,0,0,0-4.27,6.73h22.81a12.4,12.4,0,0,0-3.93-6.65,10.78,10.78,0,0,0-7.24-2.41,11.88,11.88,0,0,0-7.37,2.33m81.62-13.32a23.32,23.32,0,0,0-16.76.72,21.25,21.25,0,0,0-7.47,5.62h-.16V676h-13.87v51.74h13.87v-26q0-6.76,3.18-10.45a10.75,10.75,0,0,1,8.56-3.67q5.07,0,7.51,2.9t2.43,10v27.26H1055V697.08a34.27,34.27,0,0,0-1.34-10.14,15.48,15.48,0,0,0-9.81-10.58m29.68,47.91q4,4.37,10.84,4.37a30,30,0,0,0,7.76-.83,27.81,27.81,0,0,0,5.95-2.43l-3.57-11.07a14.42,14.42,0,0,1-6.51,1.81,3.88,3.88,0,0,1-3.5-1.61,9.26,9.26,0,0,1-1.06-5v-22h14.07V676h-14.07V657.61l-13.86,1.49V676h-6.11v11.49h6.11v23.9q0,8.49,4,12.86m30-3.24a30.6,30.6,0,0,0,21,7.61q9.78,0,14.93-4.45a14.66,14.66,0,0,0,5.14-11.64,17.55,17.55,0,0,0-.31-3.47,12.18,12.18,0,0,0-6-8.28q-2.89-1.86-11.48-5a31,31,0,0,1-5.93-2.8,3.46,3.46,0,0,1-1.83-2.79,3.21,3.21,0,0,1,1.55-2.87,6.25,6.25,0,0,1,3.52-1,16,16,0,0,1,6.23,1.37,20.5,20.5,0,0,1,6.13,4.11l6.73-10.14a29,29,0,0,0-18.94-6.52,22.63,22.63,0,0,0-9.49,1.92,15.84,15.84,0,0,0-6.67,5.45,14,14,0,0,0-2.46,8.15,17,17,0,0,0,.57,4.51,12.09,12.09,0,0,0,1.68,3.67,18.24,18.24,0,0,0,2.48,2.89,15.85,15.85,0,0,0,3.29,2.36,35.8,35.8,0,0,0,3.75,1.83c1.22.52,2.61,1,4.16,1.56,2.45.79,4.29,1.45,5.51,2a10.49,10.49,0,0,1,2.87,1.69,2.71,2.71,0,0,1,1,2.09,3.61,3.61,0,0,1-1.71,3.1,8,8,0,0,1-4.66,1.19q-7.86,0-14.33-6L1103.6,721m43.87-677.95V504.83a59.6,59.6,0,0,1-59.54,59.53H552.19V705.63H102.61a59.61,59.61,0,0,1-59.53-59.54V102.62a59.61,59.61,0,0,1,59.53-59.54ZM1190.55,0H102.61A102.61,102.61,0,0,0,0,102.62V646.09A102.61,102.61,0,0,0,102.62,748.71H585a10.27,10.27,0,0,0,10.26-10.26v-131h492.65a102.62,102.62,0,0,0,102.62-102.62V0ZM267.28,245.67A67.83,67.83,0,0,1,288.21,257a78.24,78.24,0,0,1,16.12,17.65,81.18,81.18,0,0,1,10.5,23.09,98.68,98.68,0,0,1,3.65,27.34,96.2,96.2,0,0,1-5.74,33.77A75.66,75.66,0,0,1,296.79,385a71.5,71.5,0,0,1-24.39,16.6,78.7,78.7,0,0,1-30.71,5.93q-30.13,0-47.34-20.68h-.65v44.5A20.64,20.64,0,0,1,173.05,452H150.67V244.46h43V262.9h.65q18-21.16,48.6-21.16a75.1,75.1,0,0,1,24.33,3.93m-3.85,111.21q11.47-12.19,11.48-31.56,0-20.19-11.72-32.54a38.73,38.73,0,0,0-29.35-12.4,41.31,41.31,0,0,0-16.91,3.45,39.51,39.51,0,0,0-13.23,9.39,42.86,42.86,0,0,0-8.6,14.27A49.78,49.78,0,0,0,192,325.28a46.91,46.91,0,0,0,5.52,23,38.6,38.6,0,0,0,15.08,15.48,42.53,42.53,0,0,0,21.27,5.37q18.15,0,29.59-12.2Zm231.83-18.29H382.84a40.15,40.15,0,0,0,14.68,22.47q10.8,8.18,26.2,8.33a47,47,0,0,0,27-7.74,20.33,20.33,0,0,1,25,2.37L490,377.25a77.59,77.59,0,0,1-28.55,22.05q-17.64,8.26-40.74,8.26a87,87,0,0,1-42.17-10.18,74.4,74.4,0,0,1-29.6-29.26Q338.1,349,338.08,324.49a90.54,90.54,0,0,1,6.1-33.43,78.45,78.45,0,0,1,16.6-26.31A74.26,74.26,0,0,1,386,247.83a81.68,81.68,0,0,1,31.56-6.09q23.23,0,41.21,10.5a71.72,71.72,0,0,1,27.74,29,88.21,88.21,0,0,1,9.8,41.93c0,7.59-.37,12.73-1.09,15.39m-99-51.84q-9.56,7.17-13.23,20.86h70.72q-3.36-13.16-12.2-20.61t-22.45-7.42a36.8,36.8,0,0,0-22.84,7.17M681.4,244.46V404.84H660a19.41,19.41,0,0,1-19.41-19.41h-.76q-17.22,22.14-48,22.13a78.53,78.53,0,0,1-30.71-5.94A71.19,71.19,0,0,1,536.74,385a75.54,75.54,0,0,1-16-26.14,101,101,0,0,1,0-67.28,79.42,79.42,0,0,1,15.78-26.46,71.88,71.88,0,0,1,24-17.22,72.73,72.73,0,0,1,29.92-6.25q29.34,0,47.34,20.84h.63V244.46ZM628.47,356.88q11.7-12.19,11.7-31.56t-11.48-32.06a37.16,37.16,0,0,0-28.94-12.84,39,39,0,0,0-29.44,12.36q-11.77,12.35-11.78,32.54,0,19.41,11.48,31.56t29.59,12.2a38.46,38.46,0,0,0,28.92-12.2Zm119.86,40.33a86.24,86.24,0,0,0,42.3,10.38,102.6,102.6,0,0,0,41.46-8q18.36-7.95,29.44-20.85L845,364.15c-6.59-5.81-16.28-7-23.79-2.49a49.18,49.18,0,0,1-26.41,7.17q-19.11,0-31-12.33T751.8,324.42a50.68,50.68,0,0,1,3-17.8,40.21,40.21,0,0,1,8.61-14A39,39,0,0,1,777,283.6a45.42,45.42,0,0,1,17.47-3.29,47.15,47.15,0,0,1,37.37,17.22l29-25a77,77,0,0,0-30.12-22.85,94.62,94.62,0,0,0-38-7.94q-24.51,0-43.78,10.42a74.8,74.8,0,0,0-30,29.35q-10.72,19.1-10.74,43.3t10.66,43.14a74.6,74.6,0,0,0,29.43,29.28m256.36-151.87h-.05a79.26,79.26,0,0,0-52.93,2,64.17,64.17,0,0,0-23.17,16.52h-.64V201.39a20.66,20.66,0,0,0-20.65-20.65H884.86v224.1h43V323.53q0-21.16,9.94-32.16t27.58-11q16.05.06,23.67,8.85t7.62,30.13V405h43.15V308.45a99.24,99.24,0,0,0-4.3-31,47.64,47.64,0,0,0-30.87-32.07Z"/></svg>',
            width: 100,
          },
          {
            text: "\nMerchant Dude \nMerchant Dude (Pty) Ltd \n52 Strand Street, \nWoodstock, Cape Town \n7925 \n\n\n\n\n",
          },
        ],
        {
          text: "\n\nPeach Payment Services (PTY) Ltd \nRegistration Number: 2012/076633/07 \n\n35 Brickfield Road, \n1st Floor, Unit 14, \nWoodstock, \n7925 \nRepublic of South Africa",
        },
      ],
    },
    {
      style: "tableExample",
      table: {
        widths: [120, 240, 120],
        headerRows: 1,
        body: [
          [
            { text: "Date", style: "tableHeader", alignment: "center" },
            { text: "Transaction ID", style: "tableHeader", alignment: "center" },
            { text: "Amount", style: "tableHeader", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "18-10-2022", alignment: "center" },
            { text: "001dd952909211e8adbe02d14de18c0c", alignment: "center" },
            { text: "R1200.00", alignment: "center" },
          ],
          [
            { text: "Total", style: "tableFooter", colSpan: 2, alignment: "right" },
            {},
            { text: "R1200.00", style: "tableFooter", alignment: "center" },
          ],
        ],
      },
    }
  ],
  styles: {
    tableHeader: {
      fontSize: 15,
      bold: true,
    },
    tableFooter: {
      fontSize: 15,
      bold: true,
    }
  }
};
